@echo off
REM *******************************
REM * XAMPP PHPバージョン切り替え
REM * @ version: 3.0.0
REM * @ author: y.ohta
REM * @ update: 2021.08.27
REM *******************************

SETLOCAL enabledelayedexpansion

REM 管理者権限で実行していない場合はエラー
openfiles > NUL 2>&1
IF NOT %ERRORLEVEL% EQU 0 GOTO NotAdmin

  REM 外部設定ファイルのパス定義
  SET ConfFile=%~dp0.ini\common.ini
  REM 外部設定ファイルの読み込み
  IF NOT EXIST !ConfFile! (
    ECHO [error] - "!ConfFile!" is not found
  ) ELSE (
    REM 外部設定ファイルから変数の取り出し
    FOR /f "tokens=1,* delims==" %%a IN (%ConfFile%) DO (
      SET %%a=%%b
    )

    REM 外部設定ファイルのXamppインストールディレクトリが正しいか
    IF NOT EXIST !XamppPath! (
      ECHO [error] - "!XamppPath!" is not found
      ECHO Please read "README.txt", and edit ".common.ini"
      PAUSE
    ) ELSE (
      REM PHPバージョンの選択入力要求


      ECHO ========================
      ECHO Now PHP version
      ECHO ========================
      php -v
      ECHO.
      ECHO ========================
      ECHO Choose new PHP version
      ECHO ========================
      ECHO 1: PHP 7.4.20
      ECHO 2: PHP 7.2.10
      ECHO 3: PHP 7.1.33
      ECHO 4: PHP 7.0.32
      ECHO 5: PHP 5.6.38
      ECHO C: Cancel
      ECHO.

      CHOICE /c 1234C /M "Choose the version of PHP."
      if !errorlevel! equ 1 GOTO PHP_74
      if !errorlevel! equ 2 GOTO PHP_72
      if !errorlevel! equ 3 GOTO PHP_71
      if !errorlevel! equ 4 GOTO PHP_70
      if !errorlevel! equ 5 GOTO PHP_56
      GOTO OnExit
    )
  )
  GOTO End



:NotAdmin
  ECHO.
  ECHO [ERROR] NOT ADMIN
  ECHO.
  ECHO - Please run this file with "Administor" permission
  ECHO.
  PAUSE
  GOTO End

REM PHPバージョンの切り替え

:PHP_74
  del !XamppPath!\apache\conf\extra\httpd-xampp.conf
  mklink !XamppPath!\apache\conf\extra\httpd-xampp.conf !XamppLibPath!\httpd\php-7x.conf
  rmdir !XamppPath!\php
  mklink /D !XamppPath!\php !XamppLibPath!\php\74
  GOTO OnExit

:php_72
  del !XamppPath!\apache\conf\extra\httpd-xampp.conf
  mklink !XamppPath!\apache\conf\extra\httpd-xampp.conf !XamppLibPath!\httpd\php-7x.conf
  rmdir !XamppPath!\php
  mklink /D !XamppPath!\php !XamppLibPath!\php\72
  GOTO OnExit

:PHP_71
  del !XamppPath!\apache\conf\extra\httpd-xampp.conf
  mklink !XamppPath!\apache\conf\extra\httpd-xampp.conf !XamppLibPath!\httpd\php-7x.conf
  rmdir !XamppPath!\php
  mklink /D !XamppPath!\php !XamppLibPath!\php\71
  GOTO OnExit

:PHP_70
  del !XamppPath!\apache\conf\extra\httpd-xampp.conf
  mklink !XamppPath!\apache\conf\extra\httpd-xampp.conf !XamppLibPath!\httpd\php-7x.conf
  rmdir !XamppPath!\php
  mklink /D !XamppPath!\php !XamppLibPath!\php\70
  GOTO OnExit

:PHP_56
  del !XamppPath!\apache\conf\extra\httpd-xampp.conf
  mklink !XamppPath!\apache\conf\extra\httpd-xampp.conf !XamppLibPath!\httpd\php-56.conf
  rmdir !XamppPath!\php
  mklink /D !XamppPath!\php !XamppLibPath!\php\56
  GOTO OnExit

:OnExit
  ECHO.
  ECHO ========================
  ECHO Finished.
  ECHO ========================
  ECHO Switched PHP virsion.
  ECHO Please restart XAMPP Apache.
  PAUSE
  GOTO End

:End