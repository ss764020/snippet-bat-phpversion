﻿/**
* @ title:  XAMPP PHPバージョン切り替え
* @ author: y.ohta（https://auto-you-skit.com）
* @ create: 2018.10.29
* @ update: 2021.08.27
* @ rev.:   3.0.0
**/




【0. 概要】

(0-1)
本バッチファイルの実行によってXampp Apache 上の PHPバージョンの切り替えができる



【1. 内容】

(1-1)
/phpver.bat
 … 本体ファイル


(1-2)
/.ini/common.ini
 … 共通設定ファイル

(1-3)
/xampp_lib/httpd/php-56.conf
/xampp_lib/httpd/php-7x.conf
/xampp_lib/httpd/php-74.conf
 … PHP5.6,PHP7系用のconfファイル

(1-4)
/xampp_lib/php/[56,70,71,72,74]
 … 各バージョンのPHPファイル本体



【2. 事前準備】


(2-1)
・/xampp_lib/

ディレクトリを
任意の場所
（C:\xampp_libなど）
に配置する


(2-2)
/.ini/common.ini

をテキストエディタで開き、

Xampp がインストールされているディレクトリパスと(2-1)で配置した/xampp_lib/のディレクトリパスを適宜書き換える



【3. 使用方法】

(3-1)
本プログラム（/vhost_setting_bat.bat）を管理者権限で実行する
（※ 非管理者権限だと例外処理で終了する）


(3-2)
コマンドプロンプトが立ち上がり、PHPのバージョン選択が求められるので数字を入力する


(3-3)
設定完了後、Xampp Apacheを（再）起動することでPHPのバージョン切り替えが反映される
